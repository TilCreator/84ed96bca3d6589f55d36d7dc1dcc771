# Commands to generate:
# /usr/bin/overviewer.py --config /mnt/hermitcraft-map/map.py
# /usr/bin/overviewer.py --config /mnt/hermitcraft-map/map.py --genpoi

import requests


def signFilter(poi):
    if (poi['id'] == 'Sign' or poi['id'] == 'minecraft:sign'):
        text = "\n".join([poi[f'Text{i}'] for i in range(1, 5)])
        if len(text) > 5:
            return text
    #else:
    #    __import__('pprint').pprint(poi)
    #    print("Non sign poi found: ", poi)

player_names = {}
def playerIcons(poi):
    global player_names

    if poi['id'] == 'Player':
        #__import__('pprint').pprint(poi['uuid'])
        poi['icon'] = f"https://crafatar.com/renders/head/{poi['uuid']}?overlay=true&scale=3&size=30"  # https://crafatar.com/avatars/
        if poi['uuid'] in player_names:
            name = player_names[poi['uuid']]
        else:
            while True:  # Dirty try until success
                try:
                    print(f"Getting name of {poi['uuid']}")
                    name = __import__('requests').get(f"https://playerdb.co/api/player/minecraft/{poi['uuid']}", timeout=10).json()['data']['player']['username']  # https://playerdb.co/
                except Exception:
                    __import__('traceback').print_exc()
                else:
                    break
            player_names[poi['uuid']] = name
        return f"{name} ({poi['uuid']})"


worlds['Hermitcraft 6'] = '/mnt/hermitcraft-map/source/hermitcraft6/'
worlds['Hermitcraft 5'] = '/mnt/hermitcraft-map/source/hermitcraft5/'

outputdir = '/mnt/hermitcraft-map/gen'

processes = 10


# minzoom = 0
# maxzoom = ?


default_render = {
    'texturepath': '/mnt/hermitcraft-map/minecraft/1.16.4.jar',
    #'crop': (-100, -100, 100, 100),
    'markers': [dict(name="Signs", filterFunction=signFilter, checked=False),
                dict(name="Players", filterFunction=playerIcons, checked=False)]
}

end_smooth_lighting = [Base(), EdgeLines(), SmoothLighting(strength=0.5)]
rendermodes = {
    'smooth_lighting': smooth_lighting,
    'smooth_night': smooth_night,
    'cave': cave,
    'nether_smooth_lighting': nether_smooth_lighting,
    'end_smooth_lighting': end_smooth_lighting
}

for world in worlds.keys():
    for rendermode in ['smooth_lighting', 'smooth_night', 'cave', 'nether_smooth_lighting', 'end_smooth_lighting']:
        for dimension in {'smooth_lighting': ['overworld'], 'smooth_night': ['overworld'], 'cave': ['overworld'], 'nether_smooth_lighting': ['nether'], 'end_smooth_lighting': ['end']}[rendermode]:
            for northdirection in ['upper-left', 'upper-right', 'lower-left', 'lower-right']:
                title = f'{dimension} - {rendermode} - {northdirection}'
                renders[f'{world.lower()} - {title}'] = {
                    **default_render,
                    'world': world,
                    'dimension': dimension,
                    'title': title,
                    'northdirection': northdirection,
                    'rendermode': rendermodes[rendermode]
                }

# __import__('pprint').pprint(renders)